<?php
/**
 * @file
 * Classes for object API
 */

/**
 * Object interface
 */
interface Object_Wrapper {
  /**
   * Class constructor
   *
   * Object key should be an scalar value. When the key includes multiple fields
   * it will be a string concatenating values with ':'
   *
   * @param $type string
   *   Object type
   * @param $object
   *   Object or object key.
   */
  public function __construct($type, $object);
  /**
   * Get object key / keys.
   */
  public function getKey();
  /**
   * Get field value from object/array
   */
  public function getField($name, $default = NULL);
  /**
   * Get inner object.
   */
  public function getObject();
  /**
   * Get parent object if any.
   */
  public function getParent();
  /**
   * Get aditional property
   */
  public function getProperty($name, $default = NULL);
  /**
   * Get translatable properties from table.
   */
  public function getTranslatableFields();
  /**
   * Get translatable values.
   */
  public function getTranslatableValues();
  /**
   * Get object view path
   */
  public function getPath();
  /**
   * Get object edit path
   */
  public function getEditPath();
  /**
   * Get string key for indexing.
   */
  public function getIndex();
  /**
   * Get object type name
   */
  public function getLabel();
  /**
   * Get object's language code
   */
  public function getLanguageCode();
  /**
   * Get object title / name
   */
  public function getName();
  /**
   * Get object type
   */
  public function getType();
   /**
   * Set field value to object/array
   */
  public function setField($name, $value);
  /**
   * Set aditional property
   */
  public function setProperty($name, $value);

}

/**
 * Object base class
 */
abstract class Drupal_Object implements Object_Wrapper {
  protected $type;
  protected $key;
  protected $object;
  // Aditional properties that may be set by modules
  protected $properties;

  /**
   * Class constructor
   *
   * Object key should be an scalar value. When the key includes multiple fields
   * it will be a string concatenating values with ':'
   *
   * @param $type string
   *   Object type
   * @param $object
   *   Object or object key.
   */
  public function __construct($type, $object) {
    $this->type = $type;
    if (is_scalar($object)) {
      $this->key = $object;
    }
    else {
      $this->object = $object;
    }
  }
  /**
   * Get base table name if any.
   */
  protected function getBaseTable() {
    return NULL;
  }
  /**
   * Get inner object.
   */
  public function getObject() {
    if (!isset($this->object)) {
      $this->object = $this->loadObject($this->key);
    }
    return $this->object;
  }
  /**
   * Get key from inner object.
   */
  protected function buildKey() {
    return object_key($this->type, $this->object);
  }
  /**
   * Get object key / keys.
   */
  public function getKey() {
    if (!isset($this->key)) {
      $this->key = $this->buildKey();
    }
    return $this->key;
  }
  /**
   * Get field value from object/array
   */
  function getField($name, $default = NULL) {
    if ($object = $this->getObject()) {
      return isset($object->$name) ? $object->$name : $default;
    }
  }
  /**
   * Get parent object if any.
   */
  function getParent() {
    return NULL;
  }
  /**
   * Get aditional property
   */
  function getProperty($name, $default = NULL) {
    return isset($this->properties[$name]) ? $this->properties[$name] : $default;
  }
  /**
   * Get translatable properties from table.
   */
  function getTranslatableFields() {
    $properties = array();
    if ($table = $this->getBaseTable()) {
      $schema = drupal_get_schema($table);
      foreach ($schema['fields'] as $name => $data) {
        if (!empty($data['translatable'])) {
          $properties[$name] = $name;
        }
      }
    }
    return $properties;
  }
  /**
   * Get translatable values.
   */
  function getTranslatableValues() {
    $strings = array();
    foreach (array_keys($this->getTranslatableProperties()) as $name) {
      if ($value = $this->getField($name)) {
        $strings[$name] = $value;
      }
    }
    return $strings;
  }
  /**
   * Get object view path
   */
  function getPath() {
    return NULL;
  }
  /**
   * Get object edit path
   */
  function getEditPath() {
    return NULL;
  }
  /**
   * Set field value to object/array
   */
  function setField($name, $value) {
    if ($this->getObject()) {
      $this->object->$name = $value;
    }
    return $this;
  }
  /**
   * Set aditional property
   */
  function setProperty($name, $value) {
    $this->properties[$name] = $value;
    return $this;
  }
  /**
   * Get string key for indexing.
   */
  function getIndex() {
    return $this->getType() . ':' . $this->getKey();
  }
  /**
   * Get object type information.
   */
  function getInfo($property = NULL, $default = NULL) {
    return object_type_info($this->type, $property, $default);
  }
  /**
   * Load real object or array using key.
   */
  protected abstract function loadObject($key);

  /**
   * Get object title / name
   */
  public function getLabel() {
    return t('Object');
  }
  /**
   * Get object's language code
   */
  public function getLanguageCode() {
    return $this->getField('language', LANGUAGE_NONE);
  }
  /**
   * Get object type
   */
  public function getType() {
    return $this->type;
  }
  /**
   * Get object type name
   */
  public function getName() {
    return $this->getLabel();
  }

  /**
   * Set object field
   */
  public function __set($name, $value) {
    $this->setField($name, $value);
  }
  /**
   * Get object field
   */
  public function __get($name) {
    $this->getField($name);
  }
  /**
   * Check whether object property is set
   */
  public function __isset($name) {
    $object = $this->getObject();
    return $object && isset($object->$name);
  }
  /**
   * Unset object property
   */
  public function __unset($name) {
    if ($object = $this->getObject()) {
      unset($object->$name);
    }
  }
  /**
   * Provide string information.
   */
  public function __toString() {
    return $this->getLabel() . ': ' . check_plain($this->getName()) . ' [' . $this->getIndex() . ']';
  }
  /**
   * Minimum fields we need for serialization.
   */
  public function __sleep() {
    $this->getKey();
    return array('type', 'key', 'properties');
  }
}

/**
 * Object array class.
 */
abstract class Drupal_Object_Array extends Drupal_Object implements ArrayAccess, Iterator {
  /**
   * Get field value from object/array
   */
  function getField($name, $default = NULL) {
    if ($object = $this->getObject()) {
      return isset($object[$name]) ? $object[$name] : $default;
    }
  }

  /**
   * Set field value to object/array
   */
  function setField($name, $value) {
    if ($this->getObject()) {
      $this->object[$name] = $value;
    }
    return $this;
  }
  /**
   * Check whether object property is set
   */
  public function __isset($name) {
    $object = $this->getObject();
    return $object && isset($object[$name]);
  }
  /**
   * Unset object property
   */
  public function __unset($name) {
    if ($this->getObject()) {
      unset($this->object[$name]);
    }
  }
  // ArrayAccess implementation.
  public function offsetSet($offset, $value) {
    if (is_null($offset)) {
      $this->data[] = $value;
    } else {
      $this->setField($offset, $value);
    }
  }
  public function offsetExists($offset) {
    return $this->__isset($offset);
  }
  public function offsetUnset($offset) {
    $this->__unset($offset);
  }
  public function offsetGet($offset) {
    return $this->getField($offset);
  }
  // Iterator methods.
  public function rewind() {
    $this->getObject();
    reset($this->object);
  }
  public function current() {
    $this->getObject();
    return current($this->object);
  }
  public function key() {
    if ($this->getObject()) {
      return key($this->object);
    }
    else {
      return NULL;
    }
  }
  public function next() {
    $this->getObject();
    return next($this->object);
  }
  public function valid() {
    if ($this->getObject()) {
      $key = key($this->object);
      return ($key !== NULL && $key !== FALSE);
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Entity object class.
 *
 * Object type must be entity type.
 */
class Drupal_Object_Entity extends Drupal_Object {
  /**
   * Get base table name
   */
  function getBaseTable() {
    $info = entity_get_info($this->type);
    return $info['base table'];
  }
  /**
   * Get key value from object/array
   */
  function getKey($default = NULL) {
    if (!isset($this->key)) {
      $this->key = $this->getObjectKey();
    }
    return $this->key;
  }
  /**
   * Get keys from contained object.
   */
  protected function getObjectKey() {
    $keys = entity_extract_ids($this->type, $this->object);
    return reset($keys);
  }
  /**
   * Get title from item
   */
  public function getLabel() {
    return entity_label($this->type, $this->getObject());
  }

  /**
   * Get object path
   */
  function getPath() {
    $uri = entity_uri($this->type, $this->getObject());
    return $uri['path'];
  }
  /**
   * Get object edit path
   */
  function getEditPath() {
    // This looks like a reasonable default for most objects.
    return $this->getPath() . '/edit';
  }
  /**
   * Load object
   */
  protected function loadObject($key) {
    $objects = entity_load($this->type, array($this->key));
    return reset($objects);
  }
}