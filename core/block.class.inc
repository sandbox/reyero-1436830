<?php
/**
 * Block objects
 */

/**
 * Block
 */
class Object_Block extends Drupal_Object {
  /**
   * Get object path
   */
  protected function getPath() {
    return 'admin/structure/block/manage/' . $this->getField('module') . '/' . $this->getField('delta');
  }
  /**
   * Get edit path
   */
  function getEditPath() {
    return $this->getBasePath() . '/configure';
  }
  /**
   * Load object by key
   */
  protected function loadObject($key) {
    list($module, $delta) = explode(':', $key);
    return block_load($module, $delta);
  }
}
