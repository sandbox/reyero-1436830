<?php
/**
 * Menu object wrapper.
 */

/**
 * Menu class.
 */
class Object_Menu extends Drupal_Object_Array {
  /**
   * Get Label
   */
  function getLabel() {
    return t('Menu');
  }
  /**
   * Get base table.
   */
  protected function getBaseTable() {
    return 'menu_custom';
  }
  /**
   * Get object path
   */
  function getPath() {
    return 'admin/structure/menu/manage/' . $this->getField['menu_name'];
  }
  /**
   * Get edit path
   */
  function getEditPath() {
    return $this->Path();
  }
  /**
   * Get key
   */
  function getObjectKey() {
    return $this->getField('menu_name');
  }
  /**
   * Get name, that is node title.
   */
  function getName() {
    return $this->getField('title');
  }
  /**
   * Load object by key
   */
  protected function loadObject($key) {
    return menu_load($key);
  }
}

/**
 * Menu link class.
 */
class Object_Menu_Link extends Drupal_Object_Array {
  /**
   * Get Label
   */
  function getLabel() {
    return t('Menu link');
  }
  /**
   * Get base table.
   */
  protected function getBaseTable() {
    return 'menu_links';
  }
  /**
   * Get object path
   */
  function getPath() {
    return 'admin/structure/menu/item/' . $this->getField['menu_link'];
  }
  /**
   * Get edit path
   */
  function getEditPath() {
    return $this->gePath();
  }
  /**
   * Get key
   */
  function getObjectKey() {
    return $this->getField('mlid');
  }
  /**
   * Get name, that is node title.
   */
  function getName() {
    return $this->getField('link_title');
  }
  /**
   * Get parent object that will be a menu.
   */
  function getParent() {
    return object_get('menu', $this->getField('menu_name'));
  }
  /**
   * Load object by key
   */
  protected function loadObject($key) {
    return menu_link_load($key);
  }
}