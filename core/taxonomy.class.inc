<?php
/**
 * @file
 * Object classes for taxonomy module
 */

/**
 * Taxonomy_Vocabulary
 */
class Object_Taxonomy_Vocabulary extends Drupal_Object_Entity {
  /**
   * Get name, that is node title.
   */
  function getName() {
    return $this->getField('name');
  }
}

/**
 * Taxonomy_Term
 */
class Object_Taxonomy_Term extends Drupal_Object_Entity {
  /**
   * Get name, that is node title.
   */
  function getName() {
    return $this->getField('name');
  }
  /**
   * Get parent object that will be a vocabulary.
   */
  function getParent() {
    return object_get('taxonomy_vocabulary', $this->getField('vid'));
  }
  /**
   * Load object by key
   */
  protected function loadObject($key) {
    return taxonomy_term_load($key);
  }
}