<?php
/**
 * Node Drupal objects
 */

/**
 * Wrapper for node objects
 */
class Object_Node extends Drupal_Object_Entity {
  /**
   * Get name, that is node title.
   */
  function getName() {
    return $this->getField('title');
  }
}

/**
 * Wrapper for node type object
 */
class Object_Node_type extends Drupal_Object {
  /**
   * Get base table.
   */
  protected function getBaseTable() {
    return 'node_type';
  }
  /**
   * Get object path
   */
  protected function getPath() {
    return 'admin/structure/types/manage/' . $this->getField('type');
  }
  /**
   * Get edit path
   */
  function getEditPath() {
    return $this->getPath();
  }
  /**
   * Get key
   */
  function getObjectKey() {
    return $this->getField('type');
  }
  /**
   * Get name, that is node title.
   */
  function getName() {
    return $this->getField('name');
  }
  /**
   * Load object by key
   */
  protected function loadObject($key) {
    return node_type_get_type($key);
  }
}

